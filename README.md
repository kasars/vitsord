## Vitsord
This application is meant to help teachers with grading their student's tests, by automating the calculation of the grades based on the score the student has received in the test.

The teacher enters the lowest passing grade, the highest grade and the corresponding points for the test. Then you only need to enter the score the student has received to automatically calculate the grade.

Hope this will be of some use.
  Kåre
