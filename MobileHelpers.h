/*
 * SPDX-FileCopyrightText: 2024 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
#pragma once

#include <QObject>
#include <QColor>

class MobileHelpers: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QColor statusbarColor READ statusbarColor WRITE setStatusbarColor NOTIFY statusbarColorChanged)
    Q_PROPERTY(bool systemIsDarkMode READ systemIsDarkMode NOTIFY systemIsDarkModeChanged)

public:
    explicit MobileHelpers(QObject *parent = nullptr);
    ~MobileHelpers();

    QColor statusbarColor();
    void setStatusbarColor(const QColor& color);

    bool systemIsDarkMode();

Q_SIGNALS:
    void statusbarColorChanged();
    void systemIsDarkModeChanged();

private:
    class Private;
    Private *const d;
};
