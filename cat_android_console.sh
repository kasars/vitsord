#!/bin/sh

APP_PID=$(adb shell pidof org.sars.vitsord)

adb logcat --pid $APP_PID
