/*
 * SPDX-FileCopyrightText: 2022 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
import QtQuick
import QtQuick.Window
import QtQuick.Controls

Row {
    id: labelCombo
    property string label: ""
    property alias font: labelItem.font
    property alias model: comboBox.model
    property alias textRole: comboBox.textRole
    property alias valueRole: comboBox.valueRole
    property alias currentIndex: comboBox.currentIndex
    property alias currentValue: comboBox.currentValue
    spacing: pageMargins

    signal activated(int index);

    Label {
        id: labelItem
        width: parent.width/2
        height: parent.height
        horizontalAlignment: Text.AlignRight
        verticalAlignment: Text.AlignVCenter
        text: labelCombo.label + ":"
    }

    ComboBox {
        id: comboBox
        onActivated: function (index) { labelCombo.activated(index) }
    }
}
