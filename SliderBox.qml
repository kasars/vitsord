/*
 * SPDX-FileCopyrightText: 2022 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Column {
    id: sliderBox
    property string title: "Slider"
    property alias from: slider.from
    property alias value: slider.value
    property alias to: slider.to
    spacing: pageMargins / 2

    Row {
        spacing: pageMargins
        width: sliderBox.width
        Label {
            width: parent.width/2
            text: sliderBox.title + ":"
            horizontalAlignment: Text.AlignRight
            wrapMode: Text.Wrap
        }
        Label {
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width/2
            text: ""+slider.value
            font.bold: true
        }
    }
    RowLayout {
        width: sliderBox.width
        spacing: pageMargins
        Button {
            id: decrButton
            text: "-"
            onClicked: slider.decrease()
            font.pointSize: 16
        }
        Slider {
            id: slider
            Layout.fillHeight: true
            Layout.fillWidth: true
            stepSize: 1
        }
        Button {
            id: incrButton
            text: "+"
            onClicked: slider.increase()
            font.pointSize: 16
        }
    }
}
