/*
 * SPDX-FileCopyrightText: 2022 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick
import QtCore
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Controls.Material
import sars.MobileHelpers

ApplicationWindow {
    id: vitsord
    width: 800
    height: 350
    visible: true
    title: qsTr("Vitsord")

    Material.theme: helpers.systemIsDarkMode ? Material.Dark : Material.Light

    color: bgColor
    readonly property color bgColor: helpers.systemIsDarkMode ? "black" : "white"

    MobileHelpers {
        id: helpers
        statusbarColor: vitsord.bgColor
    }

    readonly property int pageMargins: 10

    property double highGrade: 10.0
    property double lowGrade: 4.75
    property alias highPoints: highSlider.value
    property alias lowPoints: lowSlider.value

    function updateComboIndex() {
        // Highest grades are first
        for (var i=0; i<gradesModel.count; ++i) {
            var item = gradesModel.get(i);
            if (vitsord.lowGrade >= item.grade) {
                lowGradeCombo.currentIndex = i;
                break;
            }
        }
        for (var i=0; i<gradesModel.count; ++i) {
            var item = gradesModel.get(i);
            if (vitsord.highGrade >= item.grade) {
                highGradeCombo.currentIndex = i;
                break;
            }
        }
    }

    onHighGradeChanged: vitsord.updateComboIndex();
    onLowGradeChanged: vitsord.updateComboIndex();

    GridLayout {
        id: gradeRange
        anchors {
            top: parent.top
            left: parent.left
            margins: vitsord.pageMargins
        }
        columns: 2
        Label {
            Layout.alignment: Qt.AlignRight
            text: qsTr("Grades:")
        }
        Label {
            text: qsTr("%1 to %2").arg(lowGradeCombo.currentValue).arg(highGradeCombo.currentValue)
        }
        opacity: 0.5
    }

    GridLayout {
        id: pointRange
        anchors {
            top: parent.top
            right: parent.right
            margins: vitsord.pageMargins
        }
        columns: 2
        Label {
            Layout.alignment: Qt.AlignRight
            text: qsTr("Points:")
        }
        Label {
            text: qsTr("%1 to %2").arg(lowSlider.value).arg(highSlider.value)
        }
        opacity: 0.5
    }

    MouseArea {
        anchors.fill: gradeRange
        onClicked: {
            settings.state = settings.state === "" ? "edit" : ""
        }
        visible: !vitsord.bigEnough
    }
    MouseArea {
        anchors.fill: pointRange
        onClicked: {
            settings.state = settings.state === "" ? "edit" : ""
        }
        visible: !vitsord.bigEnough
    }


    Label {
        id: gradeTextMetrics
        opacity: 0
        text: "20½"
        font: gradeValueLabel.font
    }

    Row {
        id: gradeRow
        anchors {
            top: gradeRange.bottom
            left: parent.left
            right: parent.right
            margins: vitsord.pageMargins
        }
        spacing: vitsord.pageMargins
        Label {
            anchors.verticalCenter: parent.verticalCenter
            id: gradeLabel
            width :parent.width/2
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Grade:")
            font.pointSize: 18
        }
        Label {
            id: gradeValueLabel
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            text: vitsord.gradeString(gradeValueDecimal.grade)
            font.bold: true
            font.pointSize: 24
            width: gradeTextMetrics.width
        }
        Label {
            id: gradeValueDecimal
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            property double grade: vitsord.calcGrade(vitsord.highGrade, vitsord.lowGrade, vitsord.highPoints, vitsord.lowPoints, pointSlider.value)
            text: qsTr("(%1)").arg(grade.toFixed(2));
        }
    }

    SliderBox {
        id: pointSlider
        anchors {
            top: gradeRow.bottom
            left: parent.left
            right: parent.right
            margins: vitsord.pageMargins
            topMargin: vitsord.pageMargins * 2
        }
        from: 0
        value: 50
        to: Math.round(highSlider.value * 1.1)
        title: qsTr("Points")
    }

    Rectangle {
        anchors {
            right: parent.right
            bottom: settings.top
        }
        width: settingsIcon.width + 2 * vitsord.pageMargins
        height: settingsIcon.height + 2 * vitsord.pageMargins
        color: "#77FFFFFF"
        radius: vitsord.pageMargins
        Image {
            id: settingsIcon
            anchors {
                right: parent.right
                bottom: parent.bottom
                margins: vitsord.pageMargins
            }
            source: "qrc:/Settings.png"
            height: gradeLabel.font.pixelSize * 1.5
            width: height
            opacity: mArea.pressed ? 0.7 : 1.0
        }
        MouseArea {
            id: mArea
            anchors.fill: parent
            onClicked: {
                settings.state = settings.state === "" ? "edit" : ""
            }
        }
        visible: !vitsord.bigEnough
    }

    readonly property bool bigEnough: vitsord.height > 2* settings.height

    Rectangle {
        id: settings
        anchors {
            left: parent.left
            right: parent.right
        }
        color: vitsord.bgColor
        y: vitsord.bigEnough ? parent.height - settings.height : parent.height
        height: settingsSliders.height + vitsord.pageMargins * 2

        // don't let mouse clicks go through the settings rectangle
        MouseArea { anchors.fill: parent }
        Rectangle {
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }
            height: 1
            color: "#7d7d7d"
            opacity: 0.5
        }

        state: ""

        states: [
        State {
            name: "edit"
            PropertyChanges {
                settings.y: vitsord.height - settings.height
            }
        }
        ]

        Behavior on y {
            NumberAnimation { duration: 150 }
        }

        Column {
            id: settingsSliders
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                margins: vitsord.pageMargins
            }
            spacing: vitsord.pageMargins

            ListModel {
                id: gradesModel
            }

            Component.onCompleted: {
                for (var i=20; i>0; --i) {
                    gradesModel.append({ text: qsTr("%1 ½").arg(i), grade: (i+0.5) });
                    gradesModel.append({ text: qsTr("%1 +").arg(i), grade: (i+0.25) });
                    gradesModel.append({ text: qsTr("%1").arg(i),   grade: (i) });
                    gradesModel.append({ text: qsTr("%1 -").arg(i), grade: (i-0.25) });
                }
                vitsord.updateComboIndex();
            }

            LabeledComboBox {
                id: highGradeCombo
                width: parent.width
                model: gradesModel
                textRole: "text"
                valueRole: "text"
                label: qsTr("Highest grade")
                onActivated: function (index) {
                    vitsord.highGrade = gradesModel.get(index).grade;
                }
            }

            LabeledComboBox {
                id: lowGradeCombo
                width: parent.width
                model: gradesModel
                textRole: "text"
                valueRole: "text"
                label: qsTr("Lowest passing grade")
                onActivated: function (index) {
                    vitsord.lowGrade = gradesModel.get(index).grade;
                }
            }

            SliderBox {
                id: highSlider
                width: parent.width
                from: 1
                value: 50
                to: 200
                title: qsTr("Points for highest grade")
            }

            SliderBox {
                id: lowSlider
                width: parent.width
                from: 0
                value: 50
                to: highSlider.value-1
                title: qsTr("Points for lowest passing grade")
            }
        }
    }

    LicenseDialog {
        id: licenseDialog
        anchors.fill: parent
        property bool showOnStart: true
        visible: showOnStart
        onDismissed: showOnStart = false;
        darkMode: helpers.systemIsDarkMode
    }

    function calcGrade(highGrade, lowGrade, highPoints, lowPoints, points) {
        var gradeDiff = highGrade - lowGrade;
        var pointsDiff = highPoints - lowPoints;
        var gradePointRatio = gradeDiff/pointsDiff;
        var grade = ((points-lowPoints) * gradePointRatio) + lowGrade;
        return grade;
    }

    function gradeString(grade) {
        // we only want to have two digits.
        // 7.995 is should be treated as 8.00
        let gr = Math.round(grade * 100) / 100;
        var gradeRest = gr % 1;
        var symGrade = Math.floor(gr);

        var suffix = "";
        if (gradeRest >= 0.75) {
            symGrade++;
            suffix = "-";
        }
        else if (gradeRest >= 0.5) {
            suffix = "½";
        }
        else if (gradeRest >= 0.25) {
            suffix = "+";
        }
        return symGrade.toFixed(0) + suffix;
    }


    Settings {
        property alias highGrade: vitsord.highGrade
        property alias lowGrade: vitsord.lowGrade
        property alias highPoints: vitsord.highPoints
        property alias lowPoints: vitsord.lowPoints
        property alias points: pointSlider.value
        property alias showLicenseInfo: licenseDialog.showOnStart
    }

}
