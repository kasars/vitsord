/*
 * SPDX-FileCopyrightText: 2022 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material

Item {
    id: licenseDialog
    property bool darkMode: false

    signal dismissed()

    Rectangle {
        anchors.fill: parent
        color: licenseDialog.darkMode ? "#000000" : "#EEEEEE"
    }

    MouseArea { anchors.fill: parent }

    Material.theme: licenseDialog.darkMode ? Material.Dark : Material.Light

    Label {
        id: leftTextItem
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: dismissButton.bottom
            margins: pageMargins
        }
        textFormat: Text.RichText
        wrapMode: Text.Wrap
        text: qsTr("<h1>Vitsord</h1>
        I'm glad you installed this application. I hope it will be useful to you.<br>
        <br>
        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 2 of the License, or
        (at your option) any later version.<br>
        <br>
        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.<br>
        <br>
        You should have received a copy of the GNU General Public License
        along with this program.  If not, see https://www.gnu.org/licenses/.<br>
        <br>
        This application uses Qt libraries under LGPLv3. See https://www.qt.io/download-open-source")
    }

    Button {
        id: dismissButton
        anchors {
            right: parent.right
            bottom: parent.bottom
            margins: pageMargins
        }
        text: qsTr("OK")
        onClicked: {
            licenseDialog.dismissed();
        }
    }
}
